﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Configuration;
using System.IO;



public class DataImpoter : EditorWindow 
{
	string ExcelPath;
	string ExcelTextPath;
	string ExcelStringPath;
	string ExcelStringTextPath;

	int MAX_LOCALE = 6;

	[MenuItem("DataImpot/Data Impoter")]
	static void OpenWindow()
	{
		DataImpoter window = (DataImpoter)EditorWindow.GetWindow (typeof(DataImpoter));
		window.title = "DataImpoter";
	}

	public void OnGUI()
	{
		if(GUILayout.Button ("EnemyInfo Impoter"))
		{
			EnemyInfoImpoter ();
		}
		if(GUILayout.Button ("StageData Impoter"))
		{
			StageDataImpoter ();
		}
	}

	public void EnemyInfoImpoter()
	{
		string preName = "EnemyInfo";
		string prefabPath = "Assets/BundleData/" + preName + ".prefab";
		string txtPath = Application.dataPath + "/Editor/Tables/EnemyInfo";
		ExcelReader reader = new ExcelReader ();
		DataTable table = reader.ReadExcel (txtPath, preName);

		if (table == null)
			return;
		if (table.Rows.Count <= 0 || table.Columns.Count <= 0)
			return;

		List<EnemyInfo> list = new List<EnemyInfo> ();
		for(int i = 0 ; i < table.Rows.Count; i++)
		{
			EnemyInfo ei = new EnemyInfo ();
			for(int j = 0; j < table.Columns.Count; j++)
			{
				string column = table.Columns [j].ToString ();
				string str = table.Rows [i] [column].ToString ();
				if (string.IsNullOrEmpty (str))
					continue;
				if (str == "" || str == "-")
					continue;

				switch(column)
				{
				case "index":
					ei.index = int.Parse (str.ToString ());
					break;
				case "ModelName":
					ei.ModelName = str;
					break;
				case "MaterialName":
					ei.MaterialName = str;
					break;
				case "ActivePattern":
					ei.ActivePattern = (EnemyMoveType)System.Enum.Parse (typeof(EnemyMoveType), str.ToString ());
					break;
				case "MoveSpeed":
					ei.MoveSpeed = float.Parse (str.ToString ());
					break;
				case "AttackDelay":
					ei.AttackDelay = float.Parse (str.ToString ());
					break;
				case "AttackDamage":
					ei.AttackDamage = float.Parse (str.ToString ());
					break;
				case "AttackRange":
					ei.AttackRange = float.Parse (str.ToString ());
					break;
				case "BulletSpeed":
					ei.BulletSpeed = float.Parse (str.ToString ());
					break;
				case "AttackPattern":
					ei.AttackPattern = (EnemyAttackType)System.Enum.Parse (typeof(EnemyAttackType), str.ToString ());
					break;
				case "MaxHP":
					ei.MaxHp = float.Parse (str.ToString ());
					break;
				default:
					break;
				}
			}
			list.Add (ei);
		}

		GameObject obj = new GameObject (preName);
		EnemyInfoTable enemyData = obj.AddComponent<EnemyInfoTable> ();
		enemyData.SetData (list);
		if(MakePrefab (obj, prefabPath))
		{
			EditorUtility.DisplayDialog ("", "data save Successfully - " + prefabPath, "OK");
		}

		AssetImporter assetImpoter = AssetImporter.GetAtPath (prefabPath);
		assetImpoter.assetBundleName = "bundledata/" + preName;
		assetImpoter.SaveAndReimport ();

		GameObject.DestroyImmediate (obj);
		list.Clear ();

		return;
	}

	public void StageDataImpoter()
	{
		string directoryPath = Application.dataPath + "/Editor/StageData";
		string fileM = Application.dataPath + "/Editor/StageData/";
		string txtPath = ".txt";
		string[] files = System.IO.Directory.GetFiles (directoryPath);
		int count = 0;

		for(int i = 0; i < files.Length; i++)
		{
			if (files [i].Contains (".meta"))
				continue;
			if (files [i].Contains ("DS_Store"))
				continue;

			Debug.Log (files[i]);
			string fileName = files [i].Substring (fileM.Length);
			string[] fileNames = fileName.Split ('.');
			string realFileName = fileNames [0];
			StageDataImpoter (files[i], realFileName);
		}
	}

	bool StageDataImpoter(string path, string prefabName)
	{
		ExcelReader reader = new ExcelReader ();
		string prefabPath = "Assets/BundleData/" + prefabName + ".prefab";
		DataTable table = reader.ReadExcel (path, prefabName);

		if (table == null)
			return false;
		if (table.Rows.Count <= 0 || table.Columns.Count <= 0)
			return false;

		List<StageInfo> list = new List<StageInfo> ();
		for(int i = 0; i < table.Rows.Count; i++)
		{
			StageInfo si = new StageInfo ();
			for(int j = 0; j < table.Columns.Count; j++)
			{
				string column = table.Columns [j].ToString ();
				string str = table.Rows [i] [column].ToString ();

				if (string.IsNullOrEmpty (str))
					continue;
				if (str == "" || str == "-")
					continue;

				switch(column)
				{
				case "ActiveIndex":
					si.ActiveIndex = int.Parse (str.ToString ());
					break;
				case "ActiveTime":
					si.ActiveTime = float.Parse (str.ToString ());
					break;
				case "ActiveModeIndex":
					si.ActiveModelIndex = str;
					break;
				case "ActivePos":
					si.ActivePos = (EnemyActivePosType)System.Enum.Parse (typeof(EnemyActivePosType), str);
					break;
				case "ActiveLv":
					si.ActiveLv = int.Parse (str.ToString ());
					break;
				default:
					Debug.LogError ("Default : " + column + " / " + str);
					break;
				}
			}
			list.Add (si);
		}

		GameObject obj = new GameObject (prefabName);
		StageDataTable stageData = obj.AddComponent<StageDataTable> ();
		stageData.SetData (list);
		if(MakePrefab (obj, prefabPath))
		{
			EditorUtility.DisplayDialog ("", "data save Successfully - " + prefabPath, "OK");
		}

		AssetImporter assetImpoter = AssetImporter.GetAtPath (prefabPath);
		assetImpoter.assetBundleName = "bundledata/" + prefabName;
		assetImpoter.SaveAndReimport ();

		GameObject.DestroyImmediate (obj);
		list.Clear ();

		return true;
	}

	bool MakePrefab(GameObject obj, string prefabPath)
	{
		Object prefab = PrefabUtility.CreateEmptyPrefab (prefabPath);
		if (prefab == null)
			return false;
		if (PrefabUtility.ReplacePrefab (obj, prefab, ReplacePrefabOptions.ConnectToPrefab) == null)
			return false;

		return true;
	}
}
#endif