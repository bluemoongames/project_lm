﻿#if UNITY_EDITOR
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Globalization;
using System.Data;
using System.Data.Odbc;


public class ExcelReader
{
	public DataTable ReadExcel(string filePath, string SheetName)
	{
		string[] temp = filePath.Split ('/');
		string rePath = string.Empty;

		for(int i = 0; i < temp.Length -1; i++)
		{
			rePath += temp [i];
			rePath += "/";
		}

		Debug.Log (filePath + " ### " + SheetName);
		rePath += SheetName;
		rePath += ".txt";

		string[] lines = System.IO.File.ReadAllLines (rePath);
		string[] columns = lines [0].Split ('\t');

		DataTable tbl = new DataTable (SheetName);

		Debug.LogWarning (lines[0]);

		for(int col = 0; col < columns.Length; col++)
		{
			Debug.Log (columns[col]);
			tbl.Columns.Add (new DataColumn(columns[col]));
		}

		for(int i = 1; i < lines.Length; i++)
		{
			string[] rows = lines [i].Split ('\t');
			DataRow dr = tbl.NewRow ();
			for(int cIndex = 0; cIndex < rows.Length; cIndex++)
			{
				dr [cIndex] = rows [cIndex];
			}

			tbl.Rows.Add (dr);
		}
		return tbl;
	}
}
#endif