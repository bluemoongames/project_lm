﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System.Data;
using System.Text;
using System.Configuration;
using System.IO;


public class StageInfoCreateTool : MonoBehaviour 
{
	public string SavePrefabName;
	public UILabel TimeLb;
	public UILabel MovePattern;
	public UILabel modelIdx;
	public UILabel modelLv;

	float currentTime = 0.0f;
	bool bIsInit = false;

	public List<StageInfo> listStage;
	bool firstTimeInputCheck = false;

	int currentIndex;

	void Awake()
	{
		Init ();
	}
	void Start()
	{
		Init ();
	}
	void Init()
	{
		if (bIsInit)
			return;

		bIsInit = true;
		if (TimeLb != null)
			TimeLb.text = "Input To Space Key";

		if (MovePattern != null)
			MovePattern.text = "Default";
		if (modelIdx != null)
			modelIdx.text = "1001";
		if(modelLv != null)
			modelLv.text = "1";

		if (listStage == null)
			listStage = new List<StageInfo> ();

		firstTimeInputCheck = false;
		currentIndex = 0;
	}

	public void OnSave()
	{
		if (listStage == null)
			return;
		if (listStage.Count <= 0) 
		{
			Debug.LogError ("List StageInfos Count is Zero");
			return;
		}

		if (!firstTimeInputCheck) 
		{
			Debug.LogError ("Time is Null");
			return;
		}

		string prefabPath = "Assets/BundleData/" + SavePrefabName + ".prefab";

		GameObject obj = new GameObject (SavePrefabName);
		StageDataTable stageData = obj.AddComponent<StageDataTable> ();
		stageData.SetData (listStage);
		if(MakePrefab (obj, prefabPath))
		{
			EditorUtility.DisplayDialog ("", "data save Successfully - " + prefabPath, "OK");
		}

		AssetImporter assetImpoter = AssetImporter.GetAtPath (prefabPath);
		assetImpoter.assetBundleName = "bundledata/" + SavePrefabName;
		assetImpoter.SaveAndReimport ();

		GameObject.DestroyImmediate (obj);
//		listStage.Clear ();

	}

	public void OnClickPosBtn(GameObject obj)
	{
		if (!firstTimeInputCheck) 
		{
			Debug.LogError ("Time is Null");
			return;
		}

		AddWaveStep (obj.name);
	}

	public void AddWaveStep(string btnName)
	{
		currentIndex++;

		StageInfo si = new StageInfo ();
		si.ActiveIndex = currentIndex;
		si.ActiveLv = int.Parse(modelLv.text.ToString ());
		si.ActiveTime = currentTime;
		si.ActiveModelIndex = modelIdx.text;
		si.ActivePos = (EnemyActivePosType)System.Enum.Parse (typeof(EnemyActivePosType), btnName);

		listStage.Add (si);
	}

	void Update()
	{
		if (Input.GetKeyDown (KeyCode.Space)) 
		{
			if (!firstTimeInputCheck)
				firstTimeInputCheck = true;
		}

		if(Input.GetKey(KeyCode.Space))
		{
			currentTime += Time.deltaTime;
			TimeLb.text = string.Format ("{0:0#.#0}", currentTime);
		}
	}

	bool MakePrefab(GameObject obj, string prefabPath)
	{
		Object prefab = PrefabUtility.CreateEmptyPrefab (prefabPath);
		if (prefab == null)
			return false;
		if (PrefabUtility.ReplacePrefab (obj, prefab, ReplacePrefabOptions.ConnectToPrefab) == null)
			return false;

		return true;
	}
}
#endif
