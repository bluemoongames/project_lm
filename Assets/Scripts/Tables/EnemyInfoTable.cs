﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyInfoTable : MonoBehaviour 
{
	public EnemyInfo[] data;
	public void SetData(List<EnemyInfo> inData)
	{
		data = null;
		data = new EnemyInfo[inData.Count];
		for(int i = 0 ; i < inData.Count; i++)
		{
			data [i] = inData [i];
		}
	}
}
