﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageDataTable : MonoBehaviour 
{
	public StageInfo[] data;
	public void SetData(List<StageInfo> inData)
	{
		data = null;
		data = new StageInfo[inData.Count];
		for(int i = 0 ; i < inData.Count; i++)
		{
			data [i] = inData [i];
		}
	}
}
