﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EnemyAttackType
{
	Default,
	Normal,
	Circle
}

public enum EnemyMoveType
{
	Default,
	LeftToRightCurve,
	RightToLeftCurve,
	Reverse,
	LeftTopRightBottm,
	RightTopLeftBottom,
	LeftMiddleToRightBottom,
	RightMiddleToLeftBottom
}

//index	ModelName	MaterialName	ActivePattern	MoveSpeed	AttackDelay	AttackDamage	AttackRange	BulletSpeed	AttackPattern	MaxHP
[System.Serializable]
public class EnemyInfo
{
	public int index;
	public string ModelName;
	public string MaterialName;
	public EnemyMoveType ActivePattern;
	public float MoveSpeed;
	public float AttackDelay;
	public float AttackDamage;
	public float AttackRange;
	public float BulletSpeed;
	public EnemyAttackType AttackPattern;
	public float MaxHp;
}

public enum ePlayerName
{
	Player0,
	Player1,
	Player2
	
}

public enum BulletType
{
	Enemy,
	Player,
	None
}

public enum eBulletType
{
    DefaultBullet,
    Main,
    Sub,
    NormalBullet,
    SplitBullet,
    LazerBullet,
}
public enum eMuzzlFlashType
{
    eMuzzleFlashBlue,
}

public enum BulletKind
{
	Default,
	Circle,
	CircleLeftRight,
	None
}

public enum EnemyActivePosType
{
	LT,
	MT,
	RT,
	LM,
	RM,
	LB,
	MB,
	RB,
	T1,T2,T3,T4,T5,T6,T7,
	B1,B2,B3,B4,B5,B6,B7,
	L1,L2,L3,L4,L5,L6,L7,L8,L9,L10,L11,
	R1,R2,R3,R4,R5,R6,R7,R8,R9,R10,R11
}


//플레이어 데이터를 저장하는 클래스.
[System.Serializable]
public class PlayerInfo
{
	
	public string strName;
	public int nLife;
    public eBulletType eMainBulletType;
    public int nMainBulletCount;
    public int nMainBulletPower;
    public int nMainBulletSpeed;
    public float fMainBulletDelay;
    public int nSubBulletCount;
    public int nSubBulletPower;
    public int nSubBulletSpeed;
    public float fSubBulletDelay;
	public int nMoveSpeed;
	public float fDelay;
	public int nBombCount;
	
	public PlayerInfo(ePlayerName name)
	{
		strName = name.ToString();
		nLife = 3;
        eMainBulletType = eBulletType.NormalBullet;
        nMainBulletCount = 1;
		nMainBulletPower = 100;
        nMainBulletSpeed = 400;
        fMainBulletDelay = 0.2f;
        nSubBulletCount = 2;
        nSubBulletPower = 50;
        nSubBulletSpeed = 300;
        fSubBulletDelay = 1f;
		nMoveSpeed = 100;
		fDelay = 0.2f;
		nBombCount =1;

	}
}	

[System.Serializable]
public class MainData
{
	public int nPlayerIdx;
	public int nScore;
}

[System.Serializable]
public class WaveInfo
{
	public float ActiveIndex;
	public float ActiveTime;
	public float ActiveMode;
	public float ActivePos;
}

//ActiveIndex	ActiveTime	ActiveModeIndex	ActivePos	ActiveLv
[System.Serializable]
public class StageInfo
{
	public int ActiveIndex;
	public float ActiveTime;
	public string ActiveModelIndex;
	public EnemyActivePosType ActivePos;
	public int ActiveLv;
	public EnemyMoveType MovePattern;
}
public class AccountInfo : MonoBehaviour 
{
	
}
