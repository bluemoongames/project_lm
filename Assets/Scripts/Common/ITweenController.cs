﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ITweenController : MonoBehaviour {

	public static void MoveTo(GameObject obj, Vector3 location, float time, string funcName)
	{
		iTween.MoveTo(obj, iTween.Hash("position", location, "islocal", true, "time", time, "oncomplete", "EndITweenEvent", "oncompleteparams", obj));
	}
	public static void MoveTo(GameObject obj, Vector3 location, float time)
	{
		iTween.MoveTo(obj, iTween.Hash("position", location, "islocal", true, "time", time));
	}
	public static void MoveAdd(GameObject obj, Vector3 location, float time)
	{
		iTween.MoveAdd(obj, iTween.Hash("position", location, "islocal", true, "time", time));
	}
	
	void EndITweenEvent()
	{
		Debug.Log("ddd");
	}
}
