﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum eDataKey
{
	kPlayer0,
	kPlayer1,
	kPlayer2,
}

public class DataManager : Singleton<DataManager> {
	
	#region PUBLIC_METHOD
	
	public void ResetAllData()
	{
		PlayerPrefs.DeleteAll();
	}
	
	public void SetPlayerData(eDataKey key, PlayerInfo playerData)
	{
		string str = JsonUtility.ToJson(playerData);
		if (str != null && str != "")
		{
			SetString(key, str);	
		}
	}
	public PlayerInfo GetPlayerData(eDataKey key)
	{
		string str = GetString(key);
		PlayerInfo player = null;
		if (str != null)
		{
			player = JsonUtility.FromJson<PlayerInfo>(str);	
			
		}
		else
		{
			if (player == null)
			Debug.Log("<color=red>error</color>");
		}
		return player;
	}
	
	public void SetString(eDataKey key, string value)
	{
		if (value == null)
			return;
		
		PlayerPrefs.SetString(key.ToString(), value);
		PlayerPrefs.Save();
	}
	public string GetString(eDataKey key)
	{
		if (PlayerPrefs.GetString(key.ToString()) != null &&
			PlayerPrefs.GetString(key.ToString()) != "") {
			var value = PlayerPrefs.GetString (key.ToString ());
			
			if (value == null) {
				Debug.Log ("<color=red>Error</color>");
				return "";
			}
			
			return value;
			}
		return "";
	}
	/// <summary>
	/// Set FloatValue
	/// </summary>
	/// <param name="key">Key.</param>
	/// <param name="value">Value.</param>
	public void SetFloat(eDataKey key, float value)
	{
		if (value == null)
			return;
		
		PlayerPrefs.SetFloat (key.ToString(), value);
		PlayerPrefs.Save ();
	}
	
	public float GetFloat(eDataKey key)
	{
		if (PlayerPrefs.GetFloat(key.ToString()) != null) {
			var value = PlayerPrefs.GetFloat (key.ToString ());
			
			if (value == null) {
				Debug.Log ("<color=red>Error</color>");
				return 0;
			}
			
			
			return value;
		}
		return 0;
	}
	/// <summary>
	/// Set BoolValue
	/// </summary>
	/// <param name="key">Key.</param>
	/// <param name="value">If set to <c>true</c> value.</param>
	public void SetBool(eDataKey key, bool value)
	{
		int nValue = 0;
		if (value)
			nValue = 1;
		
		PlayerPrefs.SetInt (key.ToString(), nValue);
		PlayerPrefs.Save ();
	}
	public bool GetBool(eDataKey key)
	{
		bool resultValue = false;;
		
		if (PlayerPrefs.GetInt(key.ToString()) != null) {
			var value = PlayerPrefs.GetInt (key.ToString ());
			
			if (value > 0)
				resultValue = true;
			
			return resultValue;
		}
		return resultValue;
	}
	
	#endregion
	
}
