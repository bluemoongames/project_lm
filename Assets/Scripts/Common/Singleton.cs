﻿
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class SingletonInterface : MonoBehaviour
{
	public abstract void Init();
}

public class Singleton<T> : SingletonInterface where T : SingletonInterface
{
	protected static T s_Instance;

	public static T instance
	{
		get
		{
			if (s_Instance == null) 
			{
				s_Instance = FindObjectOfType (typeof(T)) as T;
			}

			if (s_Instance == null) 
			{
				GameObject container = new GameObject (typeof(T).Name);
				s_Instance = container.AddComponent (typeof(T)) as T;
				s_Instance.Init ();
			}

			return s_Instance;
		}
	}

	public virtual void OnDestroy()
	{
		s_Instance = null;
	}

	protected virtual void Awake()
	{
		if (s_Instance == null) 
		{
			s_Instance = gameObject.GetComponent<T> ();
		}
		else if (s_Instance != null) 
		{
			Debug.Log ("A Singleton already exists! Destroying new one");
			Destroy (this);
		}
	}

	public override void Init()
	{
	}
}

#region Delegate
public delegate void ResultDelegate(bool bResult);
#endregion

public class Singleton_PrefabUI_Count
{
	static Dictionary<int, GameObject> ActiveGameObjects = new Dictionary<int, GameObject>();
	public static Dictionary<int, GameObject> activeGameObjects
	{
		get { return ActiveGameObjects; }
	}

	public static bool IsExistPopupObject
	{
		get
		{
			return ActiveGameObjects.Count > 0;
		}
	}
}

public class Singleton_PrefabUI<T> : SingletonInterface where T : SingletonInterface
{
	protected static T s_Instance;
	public static T instance
	{
		get
		{
			if (s_Instance == null) 
			{
				s_Instance = FindObjectOfType (typeof(T)) as T;
			}

			if (s_Instance == null) 
			{
				GameObject container = GameObject.Instantiate (Resources.Load ("Prefabs/PopUP_UI/" + typeof(T).Name), Vector3.zero, Quaternion.identity) as GameObject;
				s_Instance = container.GetComponent (typeof(T)) as T;
				s_Instance.Init ();
			}

			return s_Instance;
		}
	}

	public void OnDestroy()
	{
		s_Instance = null;
	}

	protected virtual void Awake()
	{
		if (s_Instance == null) 
		{
			s_Instance = gameObject.GetComponent<T> ();
		}
		else if (s_Instance != this) 
		{
			Debug.Log ("A Singleton already exists! Destroying new one");
			Destroy (this);
		}
	}

	protected ResultDelegate resultDelegate;

	public override void Init()
	{
		
	}

	public virtual void OnEnable()
	{
		if(!Singleton_PrefabUI_Count.activeGameObjects.ContainsKey(gameObject.GetInstanceID()))
		{
			Singleton_PrefabUI_Count.activeGameObjects [gameObject.GetInstanceID ()] = gameObject;
		}
	}

	public virtual void OnDisable()
	{
		if(Singleton_PrefabUI_Count.activeGameObjects.ContainsKey(gameObject.GetInstanceID()))
		{
			Singleton_PrefabUI_Count.activeGameObjects.Remove(gameObject.GetInstanceID());
		}
	}

	public virtual void OpenWindow(ResultDelegate resuleDele = null)
	{
		//UIButton.DisableAll = false;

		if (gameObject.activeSelf != true)
			gameObject.SetActive (true);

		GameObject uiCamera;
		uiCamera = GameObject.FindGameObjectWithTag ("UICamera");

		if (uiCamera != null) 
		{
			gameObject.transform.parent = uiCamera.transform;
			//NGUITools.SetLayer (gameObject, uiCamera.layer);

			gameObject.transform.localPosition = Vector3.zero;
			gameObject.transform.localRotation = Quaternion.identity;
			gameObject.transform.localScale = Vector3.one;
		}

		resultDelegate = resuleDele;
	}

	public virtual void CloseWindow(bool bConfirm = true)
	{
		if (resultDelegate != null)
			resultDelegate (bConfirm);

		gameObject.SetActive (false);
	}
}







































