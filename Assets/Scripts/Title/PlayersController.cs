﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayersController : MonoBehaviour {
	
	public enum eDirection
	{
		Left,
		Right
	}
    public bool InitData = false;
	public bool m_bReadyToNext = true;//연속 버튼터치 막기
	public int m_nEndEventIdx = 0;//버튼 터치 확인용 카운트
	public int m_nPlayerIdx = 0;//플레이어 인덱스
	public List<Transform> m_lPlayers = new List<Transform>();
	public List<PlayerInfo> m_lPlayerInfo = new List<PlayerInfo>();
	
	
	#region Public_Method
	public void SetCurrentPlayerData()
	{
		MainManager.instance.sSeletedPlayerInfo = m_lPlayerInfo[m_nPlayerIdx];
	}
	public void MoveLeft()
	{
		if (m_bReadyToNext)
			m_bReadyToNext = false;
		else
			return;
		
		m_nPlayerIdx--;
		if (m_nPlayerIdx <= 0)
		{
			m_nPlayerIdx = 1;
			Debug.Log("player idx" + m_nPlayerIdx);
		}
		//MovePlayer(gameObject, eDirection.Left);
		int count = m_lPlayers.Count;
		for (int i = 0; i < count; i++) {
			Transform player = m_lPlayers[i];
			MovePlayer(player, eDirection.Left);
		}
	}
	public void MoveRight()
	{
		if (m_bReadyToNext)
			m_bReadyToNext = false;
		else
			return;
		
		m_nPlayerIdx++;
		if (m_nPlayerIdx >= 1)
		{
			m_nPlayerIdx = 0;
			Debug.Log("player idx" + m_nPlayerIdx);
		}
		//MovePlayer(gameObject, eDirection.Right);
		int count = m_lPlayers.Count;
		for (int i = 0; i < count; i++) {
			Transform player = m_lPlayers[i];
			MovePlayer(player, eDirection.Right);
		}
	}
	
	#endregion
	
	#region Private_Method
	
	void MovePlayer(Transform trans, eDirection direction)
	{
		Vector3 vDirection = Vector3.zero;
		switch (direction)
		{
		case eDirection.Left:
			vDirection = new Vector3(trans.localPosition.x + 10,trans.localPosition.y,0);	
			break;
		case eDirection.Right:
			vDirection = new Vector3(trans.localPosition.x - 10,trans.localPosition.y,0);	
			break;
		default:
			break;
		}
		iTween.MoveTo(trans.gameObject, iTween.Hash("position", vDirection, 
			"islocal", true, 
			"onComplete", "EndITweenEvent",
			"onCompleteTarget", transform.gameObject,
			"oncompleteparams", trans.gameObject,
			"time", 0.5f));
		//onComplete : 이벤트 함수이다
		//onCompleteTarget : 이벤트 함수를 가지고 있는 오브젝트
		//onCompleteparams : 이벤트 함수에 포함될 오브젝트 
	}
	void Init()
	{

        if (InitData)
        DataManager.instance.ResetAllData();
        

		if (m_lPlayers == null || m_lPlayers.Count <= 0)
		{
			int count = transform.childCount;
			for (int i = 0; i < count; i++) {
				Transform child = transform.GetChild(i);
				m_lPlayers.Add(child);
			}	
		}
		//플레이어 데이터 초기화
		if (DataManager.instance.GetPlayerData(eDataKey.kPlayer0) != null)
		{
			PlayerInfo pInfo = DataManager.instance.GetPlayerData(eDataKey.kPlayer0);
			if (m_lPlayerInfo != null)
			{
				m_lPlayerInfo.Add(pInfo);
			}
		}
		else 
		{
			PlayerInfo pInfo =new PlayerInfo(ePlayerName.Player0);
			DataManager.instance.SetPlayerData(eDataKey.kPlayer0, pInfo);
			if (m_lPlayerInfo != null)
			{
				m_lPlayerInfo.Add(pInfo);
			}
		}
		
		if (DataManager.instance.GetPlayerData(eDataKey.kPlayer1) != null)
		{
			PlayerInfo pInfo = DataManager.instance.GetPlayerData(eDataKey.kPlayer1);
			if (m_lPlayerInfo != null)
			{
				m_lPlayerInfo.Add(pInfo);
			}
		}
		else 
		{
			PlayerInfo pInfo =new PlayerInfo(ePlayerName.Player1);
			DataManager.instance.SetPlayerData(eDataKey.kPlayer1, pInfo);
			if (m_lPlayerInfo != null)
			{
				m_lPlayerInfo.Add(pInfo);
			}
		}
		
		/*if (DataManager.instance.GetPlayerData(eDataKey.kPlayer2) != null)
		{
			PlayerInfo pInfo = DataManager.instance.GetPlayerData(eDataKey.kPlayer2);
			if (m_lPlayerInfo != null)
			{
				m_lPlayerInfo.Add(pInfo);
			}
		}
		else 
		{
			PlayerInfo pInfo =new PlayerInfo(ePlayerName.Player2);
			DataManager.instance.SetPlayerData(eDataKey.kPlayer2, pInfo);
			if (m_lPlayerInfo != null)
			{
				m_lPlayerInfo.Add(pInfo);
			}
		}*/
		
		
		
		
	}
	#endregion
	
	#region EVENT_METHOD
	void EndITweenEvent(GameObject obj)
	{
		Debug.Log("event");
		if (obj.transform.localPosition.x >= 10)
		{
			obj.transform.localPosition = new Vector3(-10,obj.transform.localPosition.y,0);
		}
		else if (obj.transform.localPosition.x <= -10)
		{
			obj.transform.localPosition = new Vector3(10f,obj.transform.localPosition.y,0f);
		}
		m_nEndEventIdx++;
		if (m_nEndEventIdx >= m_lPlayers.Count)
		{
			m_nEndEventIdx=0;
			m_bReadyToNext = true;
			Debug.Log("ininini");
		}
			
	}
	#endregion
	
	#region MONO_METHOD
	void Awake(){
		Init();
	}
	void Start () {
		Init();
	}
	
	void Update () {
		
	}
	#endregion
}
