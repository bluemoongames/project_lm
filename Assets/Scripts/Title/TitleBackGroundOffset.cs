﻿using System.Collections;
using UnityEngine;

public class TitleBackGroundOffset : MonoBehaviour 
{
	public float StartY;
	public float EndY;

	void Update () 
	{
		//this.transform.Translate (0, 0, -Time.deltaTime);
		this.transform.Translate (0, -Time.deltaTime, 0);
		if (this.transform.position.y < EndY)
			this.transform.position = new Vector3 (this.transform.position.x, StartY, this.transform.position.z);
	}
}
