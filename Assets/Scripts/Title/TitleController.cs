﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
//using UnityEngine.UI;

public class TitleController : Singleton<TitleController> {

	//private GameObject m_oPlayer;
	private PlayersController m_sPlayerController;
	//private int m_nPlayerIdx = 0;
	
	#region PUBLIC_METHOD
	public void MoveLeft()
	{
		
		if (m_sPlayerController != null)
		{
			Debug.Log("moveleft");
			m_sPlayerController.MoveLeft();
		}
		else
			Debug.Log("is empty");
		
	}
	public void MoveRight()
	{
		
		if (m_sPlayerController != null)
		{
			Debug.Log("moveright");
			m_sPlayerController.MoveRight();
		}
		else
			Debug.Log("is empty");
		
	}
	public void StartGame()
	{
		//게임 실행하기전에 선택된 플레이어를 저장한다.
		m_sPlayerController.SetCurrentPlayerData();
		
//		SceneManager.LoadScene("InGame 1");
		SceneManager.LoadScene("InGame");
	}
	
	#endregion
	
	#region PRIVATE_METHOD
	
	void Init()
	{
		if (m_sPlayerController == null)
		{
			
			m_sPlayerController = GameObject.Find("Players").GetComponent<PlayersController>();
		}
		//m_nPlayerIdx = 0;
	}
	
	#endregion
	
	#region EVENT
	
	//구글 로그인하면들어온다
	void LoginCallBack(bool isSuccess)
	{
		//로그인후들어온다
		Debug.Log("Success Google Login");
		
	}
	#endregion
	
	#region MONO_METHOD
	void Awake()
	{
		Init();
	}
	void OnEnable()
	{
		Init();
	}
	void Start () {
		
		Init();
		
		//로그인
		#if UNITY_ANDROID
		GPGSManager.instance.InitializeGPGS();
		GPGSManager.instance.LoginGPGS(LoginCallBack);
		if (GPGSManager.instance.bLogin) {
			//로그인상태라면
			
		}
		#endif
		
	}
	
	void Update () {

	}
	
	void OnApplicationQuit()
	{
		#if UNITY_ANDROID
		GPGSManager.instance.LogoutGPGS();
		#endif
	}
	
	#endregion
}
