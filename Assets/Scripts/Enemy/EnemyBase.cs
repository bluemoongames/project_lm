﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBase : MonoBehaviour 
{
	bool bIsInit = false;

	public bool bIsStop;
	public bool bIsPlaying;
	public EnemyInfo enemyInfo;
	public float currentHp;
	public float maxHp;
	public float minAtk;
	public float maxAtk;
	float moveAngle;

	void Awake()
	{
		Init ();
	}

	void Start () 
	{
		Init ();
	}

	public void Init()
	{
		if (bIsInit)
			return;

		bIsInit = true;

		bIsStop = false;
		bIsPlaying = false;
		//AttackStart ();
	}

	void OnTriggerExit(Collider col)
	{
		if (col.tag == "ScreenWall") 
		{
			this.gameObject.SetActive (false);
		}
	}

	void Update () 
	{
		if (!bIsPlaying)
			return;

		if (enemyInfo == null)
			return;

		Moveing ();

		ActiveOffCheck ();
	}

	public virtual void IsDamage(float inDamage)
	{
		this.currentHp -= inDamage;
		if (this.currentHp <= 0) 
		{
			Debug.Log (this.gameObject.name + " Die.....");
			bIsPlaying = false;
            ItemManager.instance.SetItemWithTarget(this.transform);
			EffectsManager.instance.ActiveEffect ("Effect_Die", this.transform.position);
			this.gameObject.SetActive (false);

		}
	}

	void Moveing()
	{
		switch (enemyInfo.ActivePattern) 
		{
		case EnemyMoveType.Default:
			{
				this.transform.Translate (0, 0, enemyInfo.MoveSpeed * Time.deltaTime);
			}
			break;
		case EnemyMoveType.LeftToRightCurve:
			{
				//if (moveAngle >= -45f)
				//	moveAngle -= Time.deltaTime;
				this.transform.Rotate (0, -1 * Time.deltaTime * enemyInfo.MoveSpeed * 0.5f, 0);
				this.transform.Translate (0, 0, enemyInfo.MoveSpeed * Time.deltaTime);
				//this.transform.Translate (Mathf.Cos(moveAngle * Mathf.Deg2Rad) * enemyInfo.MoveSpeed * Time.deltaTime , 0, enemyInfo.MoveSpeed * Time.deltaTime);
			}
			break;
		case EnemyMoveType.RightToLeftCurve:
			{
				//if (moveAngle >= 45f)
				//	moveAngle += Time.deltaTime;
				this.transform.Rotate (0, Time.deltaTime * enemyInfo.MoveSpeed * 0.5f, 0);
				this.transform.Translate (0, 0, enemyInfo.MoveSpeed * Time.deltaTime);
				//this.transform.Translate (Mathf.Cos(moveAngle * Mathf.Deg2Rad) * enemyInfo.MoveSpeed * Time.deltaTime , 0, enemyInfo.MoveSpeed * Time.deltaTime);
			}
			break;
		}

	}

	public void ActiveOffCheck()
	{
		switch(enemyInfo.ActivePattern)
		{
		case EnemyMoveType.Default:
			{
				if(-10 >= this.transform.position.z)
				{
//					this.gameObject.SetActive (false);
				}
			}
			break;
		}
	}

	public virtual void AttackStart()
	{
		StartCoroutine (AttackEnemyWithBullet());
	}

	protected virtual IEnumerator AttackEnemyWithBullet()
	{
		while(this.gameObject.activeSelf)
		{
			BulletManager.instance.ShotBullet (
				eBulletType.DefaultBullet.ToString(), 
				this.transform.position, 
				this.transform.forward, 
				BulletType.Enemy, 
				100, 
				100,
				100);

			yield return new WaitForSeconds (enemyInfo.AttackDelay);
		}
	}

	public void SetEnemyMovePattern(EnemyMoveType mType)
	{
		if (enemyInfo == null)
			return;

		enemyInfo.ActivePattern = mType;
	}

	public void SetEnemyInfo(EnemyInfo inEnemyInfo)
	{
		if (enemyInfo == null)
			enemyInfo = new EnemyInfo ();

		enemyInfo.ActivePattern = inEnemyInfo.ActivePattern;
		enemyInfo.AttackDamage = inEnemyInfo.AttackDamage;
		enemyInfo.AttackDelay = inEnemyInfo.AttackDelay;
		enemyInfo.AttackPattern = inEnemyInfo.AttackPattern;
		enemyInfo.AttackRange = inEnemyInfo.AttackRange;
		enemyInfo.BulletSpeed = inEnemyInfo.BulletSpeed;
		enemyInfo.index = inEnemyInfo.index;
		enemyInfo.MaterialName = inEnemyInfo.MaterialName;
		enemyInfo.MaxHp = inEnemyInfo.MaxHp;
		enemyInfo.ModelName = inEnemyInfo.ModelName;
		enemyInfo.MoveSpeed = inEnemyInfo.MoveSpeed;

		this.currentHp = this.maxHp = enemyInfo.MaxHp;
		this.maxAtk = enemyInfo.AttackDamage;
		this.minAtk = enemyInfo.AttackDamage;
	}

	public void SetPlaying()
	{
		moveAngle = this.transform.rotation.eulerAngles.y;
		bIsPlaying = true;
		AttackStart ();
	}
}
