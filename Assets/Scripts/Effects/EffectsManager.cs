﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectsManager : Singleton<EffectsManager>
{
	bool bIsInit = false;

	Dictionary<string, List<GameObject>> dicEffects;
	public List<string> listEffectKeys;


	const int DefaultPoolCount = 30;

	protected override void Awake ()
	{
		Init ();
	}

	void Start () 
	{
		Init ();
	}

	public void Dummy(){}

	public override void Init ()
	{
		if (bIsInit)
			return;

		bIsInit = true;

		if (dicEffects == null) 
			dicEffects = new Dictionary<string, List<GameObject>> ();

		if (listEffectKeys == null)
			listEffectKeys = new List<string> ();

		SetDefaultEffect ();
	}
		
	void SetDefaultEffect()
	{
		AddEffect ("Effect_Dmg");
		AddEffect ("Effect_Die");
	}
	#region AddEffects
	public void AddEffect(string effectName)
	{
		if (dicEffects == null)
			dicEffects = new Dictionary<string, List<GameObject>> ();
		if (listEffectKeys == null)
			listEffectKeys = new List<string> ();

		if (dicEffects.Count <= 0) 
		{
			string prePath = "Effects/" + effectName;
			List<GameObject> tempList = new List<GameObject> ();
			for (int i = 0; i < DefaultPoolCount; i++) 
			{
				GameObject obj = GameObject.Instantiate (Resources.Load (prePath), Vector3.zero, Quaternion.identity) as GameObject;
				obj.transform.parent = this.transform;
				obj.SetActive (false);
				tempList.Add (obj);
			}
			dicEffects.Add (effectName, tempList);
			listEffectKeys.Add (effectName);
		}
		else 
		{
			if (!dicEffects.ContainsKey (effectName)) 
			{
				string prePath = "Effects/" + effectName;
				List<GameObject> tempList = new List<GameObject> ();
				for (int i = 0; i < DefaultPoolCount; i++) 
				{
					GameObject obj = GameObject.Instantiate (Resources.Load (prePath), Vector3.zero, Quaternion.identity) as GameObject;
					obj.transform.parent = this.transform;
					obj.SetActive (false);
					tempList.Add (obj);
				}

				dicEffects.Add (effectName, tempList);
				listEffectKeys.Add (effectName);
			}
		}
	}

	public void AddEffect(string effectName, int PoolCount)
	{
		if (dicEffects == null)
			dicEffects = new Dictionary<string, List<GameObject>> ();
		if (listEffectKeys == null)
			listEffectKeys = new List<string> ();

		if (dicEffects.Count <= 0) 
		{
			string prePath = "Effects/" + effectName;
			List<GameObject> tempList = new List<GameObject> ();
			for (int i = 0; i < PoolCount; i++) 
			{
				GameObject obj = GameObject.Instantiate (Resources.Load (prePath), Vector3.zero, Quaternion.identity) as GameObject;
				obj.transform.parent = this.transform;
				obj.SetActive (false);
				tempList.Add (obj);
			}

			dicEffects.Add (effectName, tempList);
			listEffectKeys.Add (effectName);
		}
		else 
		{
			if (!dicEffects.ContainsKey (effectName)) 
			{
				string prePath = "Effects/" + effectName;
				List<GameObject> tempList = new List<GameObject> ();
				for (int i = 0; i < PoolCount; i++) 
				{
					GameObject obj = GameObject.Instantiate (Resources.Load (prePath), Vector3.zero, Quaternion.identity) as GameObject;
					obj.transform.parent = this.transform;
					obj.SetActive (false);
					tempList.Add (obj);
				}

				dicEffects.Add (effectName, tempList);
				listEffectKeys.Add (effectName);
			}
		}
	}
	#endregion

	#region ActiveEffects
	public bool ActiveEffect(string effectName, Vector3 pos)
	{
		if (!dicEffects.ContainsKey (effectName))
			return false;

		for (int i = 0; i < dicEffects [effectName].Count; i++) 
		{
			if (dicEffects [effectName] [i].activeSelf)
				continue;
			dicEffects [effectName] [i].transform.position = pos;
			dicEffects [effectName] [i].SetActive (true);

			return true;
		}
		return true;
	}

	#endregion

}
