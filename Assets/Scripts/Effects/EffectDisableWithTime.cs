﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectDisableWithTime : MonoBehaviour 
{
	public float fTime;

	void OnEnable()
	{
		StartCoroutine (DisableTime ());
	}

	IEnumerator DisableTime()
	{
		float StartTime = RealTime.time;
		float EndTime = StartTime + fTime;

		while (true) 
		{
			
			if (EndTime < RealTime.time) 
			{
				this.gameObject.SetActive (false);
				break;
			}

			yield return null;
		}
	}
}
