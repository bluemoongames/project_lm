﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletTest : MonoBehaviour 
{

	float fSpeed = 100;

	public bool bIsLeft;

	void Start () 
	{
		BulletManager.instance.Dummy ();
		BulletManager.instance.SetBullet ("LeftRight", BulletKind.CircleLeftRight);

		bIsLeft = false;
	}
	

	void Update () 
	{
		if (Input.GetKeyDown (KeyCode.Space)) 
		{
			int angleMax = 30;
			for (int i = 0; i < angleMax; i++) 
			{
				Vector3 vAngle = new Vector3 (Mathf.Cos (i * (360 / angleMax) * Mathf.Deg2Rad) ,
					Mathf.Sin(i * (360 / angleMax) * Mathf.Deg2Rad), 0f);

				BulletManager.instance.ShotBullet ("LeftRight", this.transform.position, vAngle, BulletType.Player, 10, fSpeed, 500, bIsLeft);
			}

			bIsLeft = !bIsLeft;

		}	
	}
}
