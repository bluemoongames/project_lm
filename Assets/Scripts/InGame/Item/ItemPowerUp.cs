﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemPowerUp : ItemBase {

    private void Awake()
    {
        //Init();
    }
    void Start () {
        //Init();
	}
	
	// Update is called once per frame
	void Update () {
        MoveUpdate();
	}
    private void LateUpdate()
    {
        CheckRange();
    }

    public override void Init()
    {
        base.Init();
    }
    public override void ActiveItem(Transform parent)
    {
        base.ActiveItem(parent);
    }
    public override bool CheckRange()
    {
        return base.CheckRange();
    }
    public override void MoveUpdate()
    {
        base.MoveUpdate();
        if (eState == ItemState.Move)
        {
            transform.Translate(Vector3.back * speed * Time.deltaTime);
        }
    }
    public override void OnTriggerEnter(Collider col)
    {
        base.OnTriggerEnter(col);

    }

}
