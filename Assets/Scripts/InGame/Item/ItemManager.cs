﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemManager : Singleton<ItemManager> {

    const int ITEM_POWER_POOL_COUNT = 5;

    Dictionary<ItemType, List<ItemBase>> m_dicItem = new Dictionary<ItemType, List<ItemBase>>();


    bool m_bInit = false;

    float m_fDelayTime = 0.0f;

    #region MONO_METHOD
    private void Awake()
    {
        Init();
    }

    void Start()
    {
        Init();
    }


    void Update()
    {
        UpdateItem();
    }
    #endregion

    public void SetItemWithTarget(Transform startPos)
    {
        List<ItemBase> listItemBase = m_dicItem[ItemType.Power];

        for (int i = 0; i < listItemBase.Count; i++)
        {
            ItemBase ib = listItemBase[i];
            if (ib.eState == ItemBase.ItemState.Ready)
            {
                ib.ActiveItem(startPos);
                break;
            }
        }
    }

    public void Init()
    {
        if (m_bInit)
            return;

        m_bInit = true;

        SetItem(ItemType.Power);

    }

    void SetItem(ItemType itemType)
    {
        if (m_dicItem == null)
            m_dicItem = new Dictionary<ItemType, List<ItemBase>>();

        List<ItemBase> tempItemBase = new List<ItemBase>(); 

        for (int i = 0; i < ITEM_POWER_POOL_COUNT; i++)
        {
            GameObject obj = GameObject.Instantiate(Resources.Load("Items/ItemDefault"), Vector3.zero, Quaternion.identity) as GameObject;
            obj.transform.parent = transform;
            obj.transform.localPosition = new Vector3(0, 0, 500);
            ItemBase itemBase = obj.transform.GetComponent<ItemBase>();
            itemBase.eItemType = itemType;
            tempItemBase.Add(itemBase);
        }

        m_dicItem.Add(itemType, tempItemBase);

    }

    void UpdateItem()
    {
        //m_fDelayTime += Time.deltaTime;

        //if (m_fDelayTime > 5f)
        //{
        //    m_fDelayTime = 0;
        //    List<ItemBase> listItemBase = m_dicItem[ItemType.Power];

        //    for (int i = 0; i < listItemBase.Count; i++)
        //    {
        //        ItemBase ib = listItemBase[i];
        //        if (ib.eState == ItemBase.ItemState.Ready)
        //        {
        //            ib.ActiveItem();
        //            break;
        //        }
        //    }
        //}
    }



}
