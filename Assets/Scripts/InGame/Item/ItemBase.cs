﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ItemType
{
    None,
    Power,
    Gold,
}

public class ItemBase : MonoBehaviour {

    public enum ItemState
    {
        Ready,
        Move,
    }

    //[HideInInspector]
    //아이템 종류
    public ItemType eItemType = ItemType.None;

    //아이템 상태
    [SerializeField]
    public ItemState eState = ItemState.Ready;
    //시작 위치
    [System.NonSerialized]
    public Vector3 m_vStartPos = Vector3.zero;
    //움직이는 방향
    [System.NonSerialized]
    public Vector3 m_vMoveDirection = Vector3.zero;
    //아이템 이동속도.
    [System.NonSerialized]
    public float speed = 100f;
    [System.NonSerialized]
    public bool m_bInit = false;

    public virtual void ActiveItem(Transform parent)
    {
        gameObject.SetActive(true);
        gameObject.transform.localPosition = parent.position;
        eState = ItemState.Move;
    }

    public virtual void Init() {
        if (m_bInit)
            return;

        //아이템 정보 초기화
        m_bInit = true;
        eState = ItemState.Ready;
        m_vStartPos = new Vector3(0f, 70f, 450f);


        transform.localPosition = m_vStartPos;
        this.gameObject.SetActive(false);
    }
    
	public virtual void OnTriggerEnter(Collider col)
	{
        if (col.tag.Equals("Player"))
        {
            m_bInit = false;
            Init();
            Debug.Log("Item is collided");
        }

		//Debug.Log (col.tag);
		//if(!col.tag.Equals (bulletType.ToString ()))
		//{
		//	bIsShot = false;
		//	this.gameObject.SetActive (false);

		//	switch(bulletType)
		//	{
		//	case BulletType.Player:
		//		{
		//			Debug.Log ("PlayerBullet");
		//		}
		//		break;
		//	case BulletType.Enemy:
		//		{
							
		//		}
		//		break;
		//	}
		//}
	}

    public virtual void MoveUpdate() {

        //아래로만 내려온다.
        //if (eState == ItemState.Move)
        //{
        //    transform.Translate(Vector3.back * speed * Time.deltaTime);
        //}

    }
    //거리를 확인하여 화면밖으로 나가면 되돌린다.
    public virtual bool CheckRange()
    {
        if (transform.localPosition.z < -100)
        {
            m_bInit = false;
            Init();
            return true;
        }

        return false;
    }


    #region MONO_METHOD
    private void Awake()
    {
        Init();
    }
    void Start () {
        Init();
	}


    void Update(){
        //MoveUpdate();
    }
    private void LateUpdate()
    {
        //CheckRange();
    }
    #endregion
}
