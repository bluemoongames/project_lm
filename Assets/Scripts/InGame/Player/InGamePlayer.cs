﻿
#define STAND_INPUT
//#define CUSTOM_INPUT

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InGamePlayer : MonoBehaviour {

    [SerializeField]
    int m_nSpeed = 100;
    bool m_bInit = false;
    float m_fSlopeValue = 0; //이동시 회전되는 현제값.
    PlayerInfo m_sPlayerInfo; //비행기 정보를 가지고 있는 데이터 클래스
    Transform m_tBulletPos;
    GameObject m_oPlayerModel;//플레이어 모델
    Vector3 m_MoveDirection = Vector3.zero;

    public void AddPower()
    {
        if (m_sPlayerInfo.nMainBulletCount >= 5)
            return;
        Debug.Log("Power Up Player");
        m_sPlayerInfo.nMainBulletCount += 1;
    }

    //public PlayerInfo GetPlayerInfo()
    //{
    //    if (m_sPlayerInfo == null)
    //        m_sPlayerInfo = new PlayerInfo(ePlayerName.Player0);

    //    return m_sPlayerInfo;
    //}

    public Vector3 GetMoveDirection() {
        if (m_MoveDirection == null)
            m_MoveDirection = Vector3.zero; 
      
        return m_MoveDirection;
    }


    void Init() {
        if (m_bInit)
            return;

        m_bInit = true;

        m_sPlayerInfo = MainManager.instance.sSeletedPlayerInfo;
        
        if (m_sPlayerInfo == null)
        {
            //테스트용으로 데이터가 없으면 더미데이터를 만든다.
            PlayerInfo info = new PlayerInfo(ePlayerName.Player0);
            info.nMoveSpeed = 200;
            m_sPlayerInfo = info;
            m_nSpeed = info.nMoveSpeed;
            Debug.Log("TestPlayerInfo");

            //초기화한 데이터를 집어넣는다.
            InGameDataManager.instance.playerInfo = info;
        }
        if (m_sPlayerInfo != null)
        {
            m_sPlayerInfo.fMainBulletDelay = 0.2f;
            InGameDataManager.instance.playerInfo = m_sPlayerInfo;
            Debug.Log("Created Player Info" + m_sPlayerInfo.fMainBulletDelay);
        }

        if (m_oPlayerModel == null && m_sPlayerInfo != null)
        {
            string path = "Prefabs/" + m_sPlayerInfo.strName;
            Debug.Log("path : " + path);
            GameObject obj = GameObject.Instantiate(Resources.Load<GameObject>(path)) as GameObject;
            obj.transform.parent = transform;
            obj.transform.localPosition = new Vector3(0f, 0f, 0f);
            obj.transform.localScale = new Vector3(7f, 7f, 7f);
            obj.transform.localRotation = Quaternion.identity;
            obj.transform.localRotation = Quaternion.Euler(new Vector3(0f, 0f, 0f));
            obj.gameObject.name = obj.gameObject.name.Replace(obj.gameObject.name, m_sPlayerInfo.strName);
            m_oPlayerModel = obj;
            Debug.Log("Create Plane Object");
        }



    }

    //움직임에따라 비행기가 회전한다.
    void SlopeUpdate()
    {
#if CUSTOM_INPUT
		var fx = CnInputManager.GetAxis ("Horizontal");
#elif STAND_INPUT
		var fx = Input.GetAxis ("Horizontal");
#endif

        if (fx > 0 && m_fSlopeValue > -30)
        {
            m_fSlopeValue -= 2;
        }
        else if (fx < 0 && m_fSlopeValue < 30)
        {
            m_fSlopeValue += 2;
        }
        else if (fx == 0)
        {
            if (m_fSlopeValue < 0)
            {
                m_fSlopeValue += 2;
            }
            else if (m_fSlopeValue > 0)
            {
                m_fSlopeValue -= 2;
            }
        }
        transform.localRotation = Quaternion.Euler(new Vector3(0, 0, m_fSlopeValue));
    }

    void MoveUpdate()
    {
        float fX = 0;
        float fY = 0;

#if UNITY_EDITOR
        fX = Input.GetAxis("Horizontal");
        fY = Input.GetAxis("Vertical");

#else
		if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved) {

		Vector2 touchPos = Input.GetTouch(0).deltaPosition;
		fX = touchPos.x;
		fY = touchPos.y;

		}
#endif

        m_MoveDirection = new Vector3(fX, 0f, fY);
        //		Debug.Log (m_MoveDirection * Time.deltaTime);
        m_MoveDirection = transform.TransformDirection(m_MoveDirection);
        m_MoveDirection = new Vector3(m_MoveDirection.x, 0f, m_MoveDirection.z);
        //Debug.Log(m_MoveDirection);
        if (m_sPlayerInfo != null)
        {
            m_MoveDirection *= m_nSpeed;//m_sPlayerInfo.nSpeed;	
        }
        //Debug.Log("moveDirection" + m_MoveDirection);
    }
    private void Awake()
    {
        Init();
    }
    void Start () {
        Init();
	}
	
	
	void Update () {
        SlopeUpdate();
        MoveUpdate();
 
	}
}
