﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPlayerFollow : MonoBehaviour {


	public Transform target;
	public float followSpeed = 1.0f;
	public float defaultSpeed = 10.0f;
	public float damping = 1f;

	public float minZoomSpeed = 20.0f;
	public float maxZoomSpeed = 40.0f;
	public float maxZoomFactor = 0.6f;

	private Vector3 m_CurrentVelocity;

//	public Transform target;
	public bool bUpdate = true;
//	public float damping = 1;
//	public float lookAheadFactor = 3;
//	public float lookAheadReturnSpeed = 0.5f;
//	public float lookAheadMoveThreshold = 0.1f;
//
//	private float m_OffsetZ;
//	private Vector3 m_LastTargetPosition;
//	private Vector3 m_CurrentVelocity;
//	private Vector3 m_LookAheadPos;

//	void MovePlayer()
//	{
//		float xMoveDelta = (target.position - m_LastTargetPosition).x;
//
//		bool updateLookAheadTarget = Mathf.Abs(xMoveDelta) > lookAheadMoveThreshold;
//
//		if (updateLookAheadTarget)
//		{
//			m_LookAheadPos = lookAheadFactor * Vector3.right * Mathf.Sign(xMoveDelta);
//		}
//		else
//		{
//			m_LookAheadPos = Vector3.MoveTowards(m_LookAheadPos, Vector3.zero, Time.deltaTime*lookAheadReturnSpeed);
//		}
//
//		Vector3 aheadTargetPos = target.position + m_LookAheadPos + Vector3.forward*m_OffsetZ;
//		Vector3 newPos = Vector3.SmoothDamp(transform.position, aheadTargetPos, ref m_CurrentVelocity, damping);
//
//		if (newPos.x <= 60.0f && newPos.x >= -60.0f) {
//			transform.position = new Vector3 (newPos.x, transform.position.y, newPos.z);
//			m_LastTargetPosition = new Vector3 (target.position.x, transform.position.y, target.position.z);	
//		}
//	}

	void Awake()
	{
		//cam = GetComponent<tk2dCamera> ();
	}

	void Start () {
////		m_LastTargetPosition = target.position;
//		m_LastTargetPosition = new Vector3 (target.position.x, transform.localPosition.y, transform.localPosition.z);
//		m_OffsetZ = (transform.position - target.position).z;
//		transform.parent = null;
	}
	

	void Update () {
		
		if (bUpdate) {
//			MovePlayer ();
		}

	}
//	void FixedUpdate()
//	{
//		if (!bUpdate) {
////			MovePlayer ();
//		}
//	}

	void FixedUpdate()
	{
		followSpeed = transform.position.x - target.position.x;
//		if (followSpeed < transform.position.z - target.position.z) {
//			followSpeed = transform.position.z - target.position.z;	
//		}

		if (followSpeed < 0) {
			followSpeed *= -1;
			followSpeed += defaultSpeed;
		}
		Vector3 start = transform.position;
		Vector3 end = Vector3.MoveTowards (start, new Vector3 (target.localPosition.x, transform.localPosition.y, target.localPosition.z), followSpeed * Time.deltaTime);
//		Vector3 end = Vector3.SmoothDamp(transform.position,  new Vector3 (start.x, transform.localPosition.y, transform.localPosition.z), ref m_CurrentVelocity, damping);
//		end.z = start.z;
		if (end.x <= 60.0f && end.x >= -60.0f) {
			
			if (end.z >= 500) {
				end.z = 499.0f;
			}

			transform.position = end;	
		}

//		Rigidbody rigidbody = target.GetComponent<Rigidbody> ();
//		if (rigidbody != null && cam != null) {
//			float spd = rigidbody.velocity.magnitude;
//			float scl = Mathf.Clamp01 ((spd - minZoomSpeed) / (maxZoomSpeed - minZoomSpeed));
//			float targetZoomFactor = Mathf.Lerp (1, maxZoomFactor, scl);
//			cam.ZoomFactor = Mathf.MoveTowards (cam.ZoomFactor, targetZoomFactor, 0.2f * Time.deltaTime);
//		}
	}
}
