﻿


using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CnControls;


[RequireComponent(typeof(InGamePlayer))]
[RequireComponent(typeof(CharacterController))]
public class PlayerController : MonoBehaviour {
	
	[SerializeField]
    //이동하는 속도.
	private int m_nSpeed = 100;
	float m_fSlopeValue = 0;

    //초기화 확인
	private bool m_bInit = false;
    //총알 발사거리
    private int m_nRange = 400;


    //서브 미사일 발사위치.
    [SerializeField]
    private Transform[] m_oSubBulletPos; //서브 미사일 시작위치.
    //메인미사일 발사위치.
	private Transform m_BulletPos; //미사일 시작위치.
    private ParticleSystem m_muzzleEffect; //미사일 발사 이펙트.
    private InGamePlayer m_player; //플레이어 오브젝트 컨트롤 스크립트.
	private CharacterController m_Controller;

	
	public void SetPlayerModel()
	{
		
	}
	
	#region Private_Method
	
	void Init()
	{
		if (m_bInit)
			return;

		m_bInit = true;
        //Remove Sub Model
        //		transform.DetachChildren();


        if (m_player == null)
            m_player = GetComponent<InGamePlayer>();

		
		if (m_Controller == null)
		{
			m_Controller = GetComponent<CharacterController>();	
		}

		if (m_BulletPos == null) {
			
			m_BulletPos = transform.FindChild ("BulletPos");
            m_muzzleEffect = m_BulletPos.GetComponentInChildren<ParticleSystem>();
		}

        //총알을 발사한다
        StartCoroutine(CoShotBullet());
        ////서브 총알 발사한다.
        //StartCoroutine(CoShotSubBullets());

    }

    //보조미사일 발사시 사용되는 코루틴
    IEnumerator CoShotSubBullets()
    {
        while (true)
        {
            PlayerInfo playerInfo = InGameDataManager.instance.playerInfo;
            if (m_oSubBulletPos != null)
            {
                for (int i = 0; i < m_oSubBulletPos.Length; i++)
                {
                    Transform bulletPos = m_oSubBulletPos[i];
                    BulletManager.instance.ShotBullet(eBulletType.Sub.ToString(),
                        bulletPos.position,
                        bulletPos.forward,
                        BulletType.Player,
                        playerInfo.nSubBulletPower,
                        playerInfo.nSubBulletSpeed,
                        m_nRange);
                    //Debug.Log("subbullet");
                    
                }
            }
            yield return new WaitForSeconds(playerInfo.fSubBulletDelay);
        }
        
    }

    //메인 미사일 발사시 사용되는 코루틴
	IEnumerator CoShotBullet()
	{
        PlayerInfo playerInfo = InGameDataManager.instance.playerInfo;
        while (true) {
            if (m_muzzleEffect != null)
            {
                m_muzzleEffect.Stop();
                yield return !m_muzzleEffect.IsAlive();
                m_muzzleEffect.Play();
                 
            }
			if (m_BulletPos != null) 
			{
                for (int i = 0; i < playerInfo.nMainBulletCount; i++)
                {
                    int margin = 10 * i;

                    BulletManager.instance.ShotBullet(eBulletType.NormalBullet.ToString(),
                        new Vector3(m_BulletPos.position.x+ margin, m_BulletPos.position.y, m_BulletPos.position.z),
                        m_BulletPos.forward,
                        BulletType.Player,
                        playerInfo.nMainBulletPower,
                        playerInfo.nMainBulletSpeed,
                        m_nRange);
                    //Debug.Log("mainbullet delay :" + playerInfo.fMainBulletDelay);
                }
               

                //var angle = 45.0f;
                //var direction = new Vector3(Mathf.Sin(Mathf.Deg2Rad * angle), 0, Mathf.Cos(Mathf.Deg2Rad * angle));
                //BulletManager.instance.ShotBullet("Default", m_BulletPos.position, direction, BulletType.Player, m_sPlayerInfo.nPower, m_sPlayerInfo.nSpeed, 400);

                //angle = -45.0f;
                //direction = new Vector3(Mathf.Sin(Mathf.Deg2Rad * angle), 0, Mathf.Cos(Mathf.Deg2Rad * angle));
                //BulletManager.instance.ShotBullet ("Default", m_BulletPos.position, direction, BulletType.Player, m_sPlayerInfo.nPower, m_sPlayerInfo.nSpeed, 400);	
            }
            //			Debug.Log("delayyyyyyyyyyyyyy" + m_sPlayerInfo.fDelay);
            yield return new WaitForSeconds (playerInfo.fMainBulletDelay);		
		}
	}



    void MoveFixedUpdate()
    {
        Vector3 moveDirection = m_player.GetMoveDirection();
        if (m_Controller != null)
        {
            if (transform.localPosition.x >= 100 && moveDirection.x > 0)
            {
                moveDirection = new Vector3(0, 0, moveDirection.z);
            }
            else if (transform.localPosition.x <= -100 && moveDirection.x < 0)
            {
                moveDirection = new Vector3(0, 0, moveDirection.z);
            }

            if (transform.localPosition.z <= -4 && moveDirection.z < 0)
            {
                moveDirection = new Vector3(moveDirection.x, 0, 0);

            }
            else if (transform.localPosition.z >= 390 && moveDirection.z > 0)
            {
                moveDirection = new Vector3(moveDirection.x, 0, 0);
            }

            //Debug.Log(moveDirection * Time.deltaTime);
            m_Controller.Move(moveDirection * Time.deltaTime);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Item"))
        {
            ItemBase itemInfo = other.transform.GetComponent<ItemBase>();
            //파워 아이템 먹었을시
            if (itemInfo.eItemType == ItemType.Power)
            {
                m_player.AddPower();        
            }
            Debug.Log("item collid");
        }
        else if (other.tag.Equals("Bullet"))
        {
            Debug.Log("Enemy Bullet Collide");
        }
        else if (other.tag.Equals(""))
        {

        }
    }

    #endregion

    #region Mono_Method
    void Awake()
	{
		//Init();
	}
	void OnEnable()
	{
		//Init();
	}
	void Start () {
		Init();
	}

    void Update() {

    }	
	void FixedUpdate(){

		MoveFixedUpdate ();

	}


	
	#endregion
}
