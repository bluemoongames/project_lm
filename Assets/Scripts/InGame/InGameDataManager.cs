﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InGameDataManager : Singleton<InGameDataManager> {


    public PlayerInfo playerInfo = new PlayerInfo(ePlayerName.Player0);
    
    bool bInit = false;

    protected override void Awake()
    {
        base.Awake();
    }

    void Start () {
		
	}

    public override void Init()
    {
        if (bInit)
        {
            bInit = true;
        }
        base.Init();

        bInit = true;

    }

}
