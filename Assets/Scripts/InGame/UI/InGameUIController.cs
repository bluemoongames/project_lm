﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InGameUIController : Singleton<InGameUIController>
{
	public GameObject uiRoot;

	bool bIsInit = false;

	Camera uiCamera;
	GameObject MainPanel;
	UILabel timeLb;
	protected override void Awake ()
	{
		base.Awake ();
		Init ();
	}

	void Start()
	{
		Init ();
	}

	public override void Init ()
	{
		if (bIsInit)
			return;

		bIsInit = false;

		if (uiRoot == null) 
		{
			Debug.LogError ("UI Root is Null");
			return;
		}

		uiCamera = uiRoot.transform.GetChild (0).GetComponent<Camera> ();
		MainPanel = uiCamera.transform.GetChild (0).gameObject;
		timeLb = MainPanel.transform.FindChild ("TimeBackSp/TimeLb").GetComponent<UILabel> ();
	}

	public void SetTimeLb(float currentTime)
	{
		if (timeLb != null) 
		{
			timeLb.text = string.Format ("{0:0#.#0}", currentTime);
		}
	}
}
