﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBase : MonoBehaviour 
{
	protected BulletType bulletType;
	BulletKind bulletKind;
	
	public float fDamage;
	public float fSpeed;
	public float fRange;

	public bool bIsShot = false;
	bool bIsInit = false;
	public bool bisFixedUpdagte;

	public Vector3 moveDir;
	public Vector3 originPos;
	float distance;


	public string DisableEffectName;
	protected Rigidbody rig;

	protected bool bIsLeft = false;

	void Awake()
	{
		Init ();
	}

	void Start()
	{
		Init ();
	}

	public virtual void Init()
	{
		if (bIsInit)
			return;

		bIsInit = true;
		bIsShot = false;
		rig = this.GetComponent<Rigidbody> ();
	}

	void OnTriggerEnter(Collider col)
	{
		if (col.tag.Equals ("ScreenWall"))
			return;

		//Debug.Log (col.tag);
		if(!col.tag.Equals (bulletType.ToString ()))
		{
			bIsShot = false;
			this.gameObject.SetActive (false);

			switch(bulletType)
			{
			case BulletType.Player:
				{
					Debug.Log ("PlayerBullet");
					if (string.IsNullOrEmpty (this.DisableEffectName)) 
					{
						EffectsManager.instance.ActiveEffect ("Effect_Dmg", this.transform.position);
					}
					else
					{
						EffectsManager.instance.ActiveEffect (this.DisableEffectName, this.transform.position);
					}
					col.GetComponent<EnemyBase> ().IsDamage (fDamage);
				}
				break;
			case BulletType.Enemy:
				{
							
				}
				break;
			}
		}
	}

	public bool IsMoved()
	{
		return this.gameObject.activeSelf;
	}

    public void ShotBullet(Vector3 startPos, Vector3 localScale,  Vector3 shotDir, BulletType bType, float damage, float speed, float range)
    {
        if (bIsShot)
            return;

        transform.localScale = localScale;

        ShotBullet(startPos, shotDir, bType, damage, speed, range);
           
    }

	public void ShotBullet(Vector3 startPos, Vector3 shotDir, BulletType bType, float damage, float speed, float range)
	{
		if (bIsShot)
			return;

		bulletType = bType;
		fDamage = damage;
		fSpeed = speed;
		fRange = range;
		originPos = startPos;
		moveDir = shotDir;

		this.transform.position = startPos;
//		Debug.LogWarning (shotDir);
		bIsShot = true;
	}

	public void ShotBullet(Vector3 startPos, Vector3 shotDir, BulletType bType, float damage, float speed, float range, bool moveLeft)
	{
		if (bIsShot)
			return;

		bulletType = bType;
		fDamage = damage;
		fSpeed = speed;
		fRange = range;
		originPos = startPos;
		moveDir = shotDir;
		this.transform.rotation = Quaternion.identity;

		this.transform.Rotate(shotDir);
		this.transform.position = startPos;
		//if (rig != null)
		//	rig.velocity = Vector3.zero;
		//		Debug.LogWarning (shotDir);
		bIsShot = true;
		bIsLeft = moveLeft;
	}

	void Update()
	{
		if (!bIsShot)
			return;
		if (bisFixedUpdagte)
			return;

		MoveedBullet ();
	}

	void FixedUpdate()
	{
		if (!bIsShot)
			return;
		if (!bisFixedUpdagte)
			return;

		MoveedBullet ();
	}

	protected virtual void MoveedBullet()
	{
		if (!bIsShot)
			return;
		if(bisFixedUpdagte)
		{
			this.transform.position += moveDir * (fSpeed * Time.fixedDeltaTime);
		}
		else
		{
			this.transform.position += moveDir * (fSpeed * Time.deltaTime);
		}

		if(!MoveedWithRangeCheck ())
		{
			bIsShot = false;
			this.gameObject.SetActive (false);
		}
	}

	protected bool MoveedWithRangeCheck()
	{
		distance = Vector3.Distance (originPos, this.transform.position);
		if (Mathf.Abs (distance) >= fRange)
			return false;

		return true;
	}
}
