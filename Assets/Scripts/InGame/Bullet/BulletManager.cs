﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletManager : Singleton<BulletManager>
{

	const int BULLET_DEFAULT_POOL_COUNT = 100;
    const int BULLET_MAIN_BULLET_POOL_COUNT = 100;
    const int BULLET_SUB_BULLET_POOL_COUNT = 100;

    bool bIsInit = false;

	Dictionary<string, List<BulletBase>> dicBullets;
	List<string> listBulletNames;

	public GameObject objPlayer;

	void Awake()
	{
		Init ();
	}

	void Start()
	{
		Init ();
	}
	public void Dummy(){}
	public void Init()
	{
		if (bIsInit)
			return;

		bIsInit = true;

		SetDefaultBullet ();

		if (objPlayer == null) {
			Debug.LogWarning ("objPlayer is Empty");
			objPlayer = GameObject.Find ("Player/BulletPos") as GameObject;	
		}

	}

	public void SetDefaultBullet()
	{
		SetBullet (eBulletType.DefaultBullet.ToString());
        SetBullet(eBulletType.NormalBullet.ToString(), BULLET_MAIN_BULLET_POOL_COUNT);
        //SetBullet(eBulletType.Sub.ToString(), BULLET_SUB_BULLET_POOL_COUNT);
        //SymbolMissileBlue - 일반 미사일 이름.

        //		tempBullets.Clear ();
        //		tempBullets = null;
    }

    public void SetBullet(string name, int currentBulletCount)
    {
        if (dicBullets == null)
            dicBullets = new Dictionary<string, List<BulletBase>>();

        if (listBulletNames == null)
            listBulletNames = new List<string>();

        if (dicBullets.ContainsKey(name))
            return;

        List<BulletBase> tempBullets = new List<BulletBase>();

        for (int i = 0; i < currentBulletCount; i++)
        {
            GameObject obj = GameObject.Instantiate(Resources.Load("Bullets/"+ name), Vector3.zero, Quaternion.identity) as GameObject;
            obj.transform.parent = this.transform;
            obj.transform.localScale = new Vector3(3f, 3f, 3f);
            BulletBase bullet = obj.AddComponent<BulletBase>();
            bullet.gameObject.SetActive(false);

            tempBullets.Add(bullet);
        }

        dicBullets.Add(name, tempBullets);
        listBulletNames.Add(name);
    }

    public void SetBullet(string name)
	{
		if (dicBullets == null)
			dicBullets = new Dictionary<string, List<BulletBase>> ();

		if (listBulletNames == null)
			listBulletNames = new List<string> ();

		if (dicBullets.ContainsKey (name))
			return;

		List<BulletBase> tempBullets = new List<BulletBase> ();

		for(int i = 0; i < BULLET_DEFAULT_POOL_COUNT; i++)
		{
			GameObject obj = GameObject.Instantiate (Resources.Load ("Bullets/"+ name), Vector3.zero, Quaternion.identity) as GameObject;
			obj.transform.parent = this.transform;
			BulletBase bullet = obj.AddComponent<BulletBase> ();
			bullet.gameObject.SetActive (false);

			tempBullets.Add (bullet);
		}

		dicBullets.Add (name, tempBullets);
		listBulletNames.Add (name);
	}

	public void SetBullet(string name, BulletKind kind )
	{
		if (dicBullets == null)
			dicBullets = new Dictionary<string, List<BulletBase>> ();
		if (listBulletNames == null)
			listBulletNames = new List<string> ();

		if (dicBullets.ContainsKey (name))
			return;

		List<BulletBase> tempBullets = new List<BulletBase>();

		for(int i = 0; i < BULLET_DEFAULT_POOL_COUNT; i++)
		{
			GameObject obj = GameObject.Instantiate (Resources.Load ("Bullets/BulletDefault"), Vector3.zero, Quaternion.identity) as GameObject;
			obj.transform.parent = this.transform;
			BulletBase bullet;
			switch (kind) 
			{
			case BulletKind.Default:
				{
					bullet = obj.AddComponent<BulletBase> ();
					bullet.gameObject.SetActive (false);

					tempBullets.Add (bullet);
				}
				break;
			case BulletKind.Circle:
				{
					
				}
				break;
			case BulletKind.CircleLeftRight:
				{
					bullet = obj.AddComponent<BulletMoveToLeftRight> ();
					bullet.gameObject.SetActive (false);

					tempBullets.Add (bullet);
				}
				break;
			default:
				{
					bullet = obj.AddComponent<BulletBase> ();
					bullet.gameObject.SetActive (false);

					tempBullets.Add (bullet);
				}
				break;
			}

		}

		dicBullets.Add (name, tempBullets);
		listBulletNames.Add (name);
	}

	public void SetBulletWithPathAndBulletCount(string bulletPath, int bulletCount)
	{
		
	}

	public void ShotBullet(string bulletName, Vector3 startPos, Vector3 shotDir, BulletType bType, float damage, float speed, float range)
	{
		if (!dicBullets.ContainsKey (bulletName))
			return;

		for(int i = 0; i < dicBullets[bulletName].Count; i++)
		{
			if (dicBullets [bulletName] [i].gameObject.activeSelf)
				continue;
			if (dicBullets [bulletName] [i].bIsShot)
				continue;
			//Debug.Log (i);
			dicBullets [bulletName] [i].ShotBullet (startPos, shotDir, bType, damage, speed, range);
			dicBullets [bulletName] [i].gameObject.SetActive (true);
			break;
		}
	}
	public void ShotBullet(string bulletName, Vector3 startPos, Vector3 shotDir, BulletType bType, float damage, float speed, float range, bool moveLeft)
	{


		if (!dicBullets.ContainsKey (bulletName))
			return;

		for(int i = 0; i < dicBullets[bulletName].Count; i++)
		{
			if (dicBullets [bulletName] [i].gameObject.activeSelf)
				continue;
			if (dicBullets [bulletName] [i].bIsShot)
				continue;
			//Debug.Log (i);
			dicBullets [bulletName] [i].ShotBullet (startPos, shotDir, bType, damage, speed, range, moveLeft);
			dicBullets [bulletName] [i].gameObject.SetActive (true);
			break;
		}
	}

	void Update()
	{
		if (Input.GetKeyDown (KeyCode.X))
		{
//			Debug.Log ("Player Shot!!!!");
			if (objPlayer != null) {
				ShotBullet ("Default", objPlayer.transform.position, objPlayer.transform.forward, BulletType.Player, 100, 100, 400);
			}
		}
	}
}

