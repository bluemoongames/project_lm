﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletHoming : BulletBase {

    [SerializeField]
    private Transform targetTrans;

    private Vector3 homingPos;
    private Vector3 beforeDirVec;
    private Vector3 afterDirVec;

    private float betweenAngle = 0.0f;

	
	void Start () {
        //StartCoroutine(CoFindTarget());
        //FindTarget();
        //StartCoroutine(CoMoveProcess());
        //StartCoroutine(CoHomingProcess());
       
    }
	
	
	void Update () {
		
	}

    public new void ShotBullet(Vector3 startPos, Vector3 shotDir, BulletType bulletType, float damage, float speed, float range)
    {
        FindEnemyTarget();
        base.ShotBullet(startPos, shotDir, bulletType, damage, speed, range);

    }

    IEnumerator CoMoveProcess()
    {
        while (true)
        {
            Moving();
            yield return null;
        }
    }
    IEnumerator CoHomingProcess()
    {
        yield return targetTrans;
        while (true)
        {       
            homingPos = targetTrans.position;

            beforeDirVec = targetTrans.position - transform.position;
            yield return new WaitForSeconds(0.1f);
            afterDirVec = targetTrans.position - transform.position;
            CalcBetweenAngel();
            if (betweenAngle > 60f) homingPos = targetTrans.position;
          
        }
    }
    IEnumerator CoFindTarget()
    {
        yield return new WaitForSeconds(0.5f);
        //for (int i = 0; i < EnemyController.instance.listEnemys.Count; i++)
        //{
        //    var str = EnemyController.instance.listEnemys[i];
        //    Debug.Log("enemy name : " + str);
        //}

        foreach (var item in EnemyController.instance.dicEnemys)
        {
            var dicdata = item.Value;
            foreach (var item1 in dicdata)
            {
                var enemybase = item1;
                Debug.Log("enemybaseName :" + enemybase.name);
                if (enemybase.bIsPlaying)
                {
                    FindNearTarget(enemybase.transform);
                }
                
            }
        }
        yield return targetTrans;
        StartCoroutine(CoMoveProcess());
        StartCoroutine(CoHomingProcess());

        //FindTarget();
    }

    void FindEnemyTarget()
    {
        foreach (var item in EnemyController.instance.dicEnemys)
        {
            var dicdata = item.Value;
            foreach (var item1 in dicdata)
            {
                var enemybase = item1;
                Debug.Log("enemybaseName :" + enemybase.name);
                if (enemybase.bIsPlaying)
                {
                    FindNearTarget(enemybase.transform);
                }

            }
        }
    }

    void FindNearTarget(Transform target)
    {
      
        float minDistance = 500;


        //for (int i = 0; i < targets.Length; i++)
        //{
            Debug.Log("FindingTarget");
            var col = target;
            var distance = (transform.position - col.position).sqrMagnitude;
            if (minDistance <= distance)
            {
                minDistance = distance;
                targetTrans = col.transform;
                Debug.Log("FindTarget!!!!!!!!!!");
                
            }
        //}
    }

    void Moving()
    {
        transform.position = Vector3.Lerp(transform.position, homingPos, Time.deltaTime);

    }

    void CalcBetweenAngel()
    {
        float dotValue = (beforeDirVec.x * afterDirVec.x) +
            (beforeDirVec.y * afterDirVec.y) +
            (beforeDirVec.z * afterDirVec.z);

        float magnitude = beforeDirVec.magnitude * afterDirVec.magnitude;

        //Debug.Log(dotValue / magnitude);
        betweenAngle = Mathf.Acos(dotValue / magnitude) * Mathf.Rad2Deg;
        //Debug.Log("angle is : " + betweenAngle);

    }

}
