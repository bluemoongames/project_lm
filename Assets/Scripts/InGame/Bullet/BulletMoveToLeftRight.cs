﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMoveToLeftRight : BulletBase 
{
	bool bIsForce = false;

	public override void Init ()
	{
		base.Init ();
		//bisFixedUpdagte = true;
	}

	protected override void MoveedBullet ()
	{
		if (!bIsShot)
			return;
		
		if(bisFixedUpdagte)
		{
			/*this.transform.position += moveDir * (fSpeed * Time.fixedDeltaTime);
			if (bIsLeft) 
			{
				if (!bIsForce)
				{
					bIsForce = true;
					rig.AddRelativeForce (Vector3.left * fSpeed, ForceMode.VelocityChange);
					//rig.velocity = Vector3.left * fSpeed;
				}
			}
			else
			{
				//this.transform.position += Vector3.right * fSpeed * Time.fixedDeltaTime;
				if (!bIsForce)
				{
					bIsForce = true;
					rig.AddRelativeForce (Vector3.right * fSpeed, ForceMode.Acceleration);
					//rig.velocity = Vector3.right * fSpeed;
				}
			}
			*/
		}
		else
		{
			//Debug.LogError ("#####");
			moveDir = this.transform.forward * fSpeed * Time.deltaTime;
			this.transform.Translate (this.transform.forward * fSpeed * Time.deltaTime);
			if (bIsLeft) 
			{
				//this.transform.Rotate (new Vector3(10f* Mathf.Deg2Rad, 0f,0f));
				//this.transform.Translate (Vector3.left * fSpeed * Time.deltaTime);
				//Vector3 local = this.transform.localPosition;
				//local.z -= Time.deltaTime * fSpeed;
				//this.transform.localPosition = local;
				//this.transform.position += Vector3.left * fSpeed * Time.fixedDeltaTime;
				//moveDir.x -= 1.0f;
				//this.transform.position += moveDir * (fSpeed * Time.deltaTime);
				/*if (!bIsForce)
				{
					bIsForce = true;
					rig.AddRelativeForce (Vector3.left * 10, ForceMode.Impulse);
					rig.velocity = Vector3.left * fSpeed;
				}*/
				//bIsLeft = false;
			}
			else
			{
				//this.transform.Rotate (new Vector3(10f* Mathf.Deg2Rad,0f,0f));
				//this.transform.localPosition = local;
				//this.transform.Translate (Vector3.right * fSpeed * Time.deltaTime);
				//moveDir.x += 1.0f;
				//this.transform.position += Vector3.right * fSpeed * Time.fixedDeltaTime;
				//this.transform.position += moveDir * (fSpeed * Time.deltaTime);
				/*if (!bIsForce)
				{
					bIsForce = true;
					rig.AddRelativeForce (Vector3.right * 10, ForceMode.Impulse);
					//rig.velocity = Vector3.right * fSpeed;
				}*/
				//bIsLeft = true;
			}
		}

		if(!MoveedWithRangeCheck ())
		{
			bIsForce = false;
			bIsShot = false;
			this.gameObject.SetActive (false);
		}
	}

}
