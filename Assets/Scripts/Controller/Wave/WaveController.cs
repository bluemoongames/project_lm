﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveController : Singleton<WaveController>
{
	bool bIsInit = false;

	bool bIsWaveStart = false;

	List<StageInfo> listStageInfos;

	public StageInfo currentStageInfo;
	int currentIndex;

	Dictionary<EnemyActivePosType, GameObject> enemyActivePos;
	WaitForSeconds ws;
	void Awake(){ Init (); }

	void Start () { Init (); }

	public void Init()
	{
		if (bIsInit)
			return;

		bIsInit = true;

		WavePoint[] wavePoints = this.gameObject.transform.GetComponentsInChildren<WavePoint> ();
		if (wavePoints.Length >= 0) 
		{
			if (enemyActivePos != null)
				return;

			enemyActivePos = new Dictionary<EnemyActivePosType, GameObject> ();

			for (int i = 0; i < wavePoints.Length; i++) 
			{
				enemyActivePos.Add (wavePoints [i].PosType, wavePoints [i].gameObject);
			}
		}
	}

	public void WaveStart()
	{
		if (bIsWaveStart)
			return;

		bIsWaveStart = true;
	}

	public void StageStart(List<StageInfo> inList)
	{
		CopyStageInfos (inList, out listStageInfos);
//		Debug.LogError (listStageInfos);
		currentIndex = 0;

		if (listStageInfos != null)
			currentStageInfo = listStageInfos [currentIndex];

		StageWaveStart ();
	}

	public void StageWaveStart()
	{
		if (currentStageInfo == null)
			return;

		StartCoroutine (Waveing());
	}

	IEnumerator Waveing()
	{
		Debug.LogError ("Wave START");
		float currentTime = 0.0f;
		ws = new WaitForSeconds (Time.deltaTime);
		while (true) 
		{
			InGameUIController.instance.SetTimeLb (currentTime);
			if (currentTime >= currentStageInfo.ActiveTime) 
			{
				EnemyController.instance.ActiveEnemy (currentStageInfo, enemyActivePos [currentStageInfo.ActivePos].transform.position, enemyActivePos [currentStageInfo.ActivePos].transform.rotation);

				currentIndex++;
				currentStageInfo = listStageInfos [currentIndex];
			}
			if (currentIndex >= listStageInfos.Count) 
			{
				StageClearCheck ();
				break;
			}
			currentTime += Time.deltaTime;
			yield return ws;
		}
	}

	public void StageClearCheck()
	{
		Debug.LogWarning ("StageClearCheck");
	}



	public void CopyStageInfos(List<StageInfo> inList, out List<StageInfo> outList)
	{
		outList = new List<StageInfo> ();

		for (int i = 0; i < inList.Count; i++) 
		{
			StageInfo si = new StageInfo ();
			si.ActiveIndex = inList [i].ActiveIndex;
			si.ActiveLv = inList [i].ActiveLv;
			si.ActiveModelIndex = inList [i].ActiveModelIndex;
			si.ActivePos = inList [i].ActivePos;
			si.ActiveTime = inList [i].ActiveTime;
			si.MovePattern = inList [i].MovePattern;
			outList.Add (si);
		}
	}

}
