﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainManager : Singleton<MainManager> 
{
	public PlayerInfo sSeletedPlayerInfo;
	public string LoadStageInfo;
	bool bIsInit = false;

	protected override void Awake ()
	{
		base.Awake ();
		Init ();
	}

	void Start()
	{
		Init ();
	}

	public override void Init ()
	{
		base.Init ();

		if (bIsInit)
			return;

		bIsInit = true;
		LoadStageInfo = string.Empty;
	}

	public void Dummy(){}

	void OnEnable()
	{
		DontDestroyOnLoad(this.gameObject);
	}
}
