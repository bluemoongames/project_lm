﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InGameController : Singleton<InGameController> 
{
	const int DEFAULT_LOAD_ENEMY_MODELS = 10;


	bool bIsInit = false;

	bool bisStop;

	public List<StageInfo> listStageInfo;
	public List<string> listModelIndex;

	public bool bIsStop
	{
		get{return bisStop;}
		set{
			bisStop = value;
			if (bisStop == true)
				Time.timeScale = 0.0f;
			else
				Time.timeScale = 1.0f;
		}
	}

	ScrollMap scrollMapObj;
	void Awake()
	{
		Init ();
	}
	void Start()
	{
		Init ();
	}

	public void Init()
	{
		if (bIsInit)
			return;

		bIsInit = true;

		scrollMapObj = GameObject.Find ("ScrollMap").GetComponent<ScrollMap> ();
		if(scrollMapObj != null)
		{
			scrollMapObj.ScrollingMap (true);
		}

        ItemManager.instance.Init();
		BulletManager.instance.Init ();
		MainManager.instance.Dummy ();

		if (string.IsNullOrEmpty (MainManager.instance.LoadStageInfo))
			MainManager.instance.LoadStageInfo = "Stage1_N";

		EffectsManager.instance.Dummy ();

		LoadStage ();
	}

	public void LoadStage()
	{
		string loadPath = "BundleData/" + MainManager.instance.LoadStageInfo;
		Object loadObject = Resources.Load (loadPath);
		GameObject loadGameObject = loadObject as GameObject;

		StageDataTable sdt = loadGameObject.GetComponent<StageDataTable> ();

		if (listStageInfo == null)
			listStageInfo = new List<StageInfo> ();

		for(int i = 0 ; i < sdt.data.Length; i++)
		{
			StageInfo si = new StageInfo ();
			si.ActiveIndex = sdt.data [i].ActiveIndex;
			si.ActiveLv = sdt.data [i].ActiveLv;
			si.ActiveModelIndex = sdt.data [i].ActiveModelIndex;
			si.ActivePos = sdt.data [i].ActivePos;
			si.ActiveTime = sdt.data [i].ActiveTime;
			si.MovePattern = sdt.data [i].MovePattern;

			listStageInfo.Add (si);
		}

		if (listStageInfo != null && listStageInfo.Count > 0)
			LoadModel ();
	}

	public void LoadModel()
	{
		if (listModelIndex == null)
			listModelIndex = new List<string> ();
		else
			listModelIndex.Clear ();
		
		for(int i = 0 ; i < listStageInfo.Count; i++)
		{
			string modelName = listStageInfo [i].ActiveModelIndex;
			if (listModelIndex.Count == 0)
				listModelIndex.Add (modelName);
			else
			{
				if(!listModelIndex.Contains (modelName))
					listModelIndex.Add (modelName);
			}
		}

		string loadPath = "BundleData/EnemyInfo";
		Object loadObject = Resources.Load (loadPath);
		GameObject loadGameObject = loadObject as GameObject;

		EnemyInfoTable edt = loadGameObject.GetComponent<EnemyInfoTable> ();
		for(int i = 0; i < edt.data.Length; i++)
		{
			for(int j = 0; j < listModelIndex.Count; j++)
			{
				if (edt.data [i].index.ToString () == listModelIndex [j].ToString ())
					EnemyController.instance.AddEnemy (edt.data [i].ModelName, (EnemyInfo)edt.data [i]);
			}
		}

		WaveController.instance.StageStart (listStageInfo);
	}

	void Update()
	{
//		if (Input.GetKeyDown (KeyCode.Space))
//			ReturnWithTitle ();
	}

	public void GamePause()
	{
		bIsStop = true;
	}

	public void GameResume()
	{
		bIsStop = false;
	}

	public void ReturnWithTitle()
	{
		UnityEngine.SceneManagement.SceneManager.LoadScene ("Title");
	}
}
