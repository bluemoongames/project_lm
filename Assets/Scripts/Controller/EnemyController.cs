﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : Singleton<EnemyController> 
{
	const int DEFAULT_LOAD_ENEMY_MODELS_COUNT = 10;

	bool bIsInit = false;

	public Dictionary<string, List<EnemyBase>> dicEnemys;
	public List<string> listEnemys;
	void Awake(){ Init (); }

	void Start () { Init (); }

	public void Init()
	{
		if (bIsInit)
			return;

		bIsInit = true;
	}

#region ADD_ENEMY
	public void AddEnemy(string enemyName, EnemyInfo enemyInfo)
	{
		bool result = false;

		if (dicEnemys == null)
			dicEnemys = new Dictionary<string, List<EnemyBase>> ();
		if (listEnemys == null)
			listEnemys = new List<string> ();
		
		if(dicEnemys.Count <= 0)
		{
			List<EnemyBase> tempBase = new List<EnemyBase> ();
			for(int i = 0; i < DEFAULT_LOAD_ENEMY_MODELS_COUNT; i++)
			{
				string modelPath = "Models/" + enemyName;
				GameObject newEnemy = GameObject.Instantiate (Resources.Load (modelPath), Vector3.zero, Quaternion.identity) as GameObject;
				newEnemy.transform.parent = this.transform;
				EnemyBase enemy = newEnemy.AddComponent<EnemyBase> ();
				enemy.SetEnemyInfo (enemyInfo);
				tempBase.Add (enemy);
				enemy.gameObject.SetActive (false);
			}
			listEnemys.Add (enemyInfo.index.ToString());
			dicEnemys.Add (enemyInfo.index.ToString(), tempBase);
		}
		else
		{
			if (dicEnemys.ContainsKey (enemyName))
			{
				
			}
			else
			{
				List<EnemyBase> tempBase = new List<EnemyBase> ();
				for(int i = 0; i < DEFAULT_LOAD_ENEMY_MODELS_COUNT; i++)
				{
					string modelPath = "Models/" + enemyName;
					GameObject newEnemy = GameObject.Instantiate (Resources.Load (modelPath), Vector3.zero, Quaternion.identity) as GameObject;
					newEnemy.transform.parent = this.transform;
					EnemyBase enemy = newEnemy.AddComponent<EnemyBase> ();
					enemy.SetEnemyInfo (enemyInfo);
					tempBase.Add (enemy);
					enemy.gameObject.SetActive (false);
				}
				listEnemys.Add (enemyInfo.index.ToString());
				dicEnemys.Add (enemyInfo.index.ToString(), tempBase);
			}
		}
	}
#endregion

	public void ActiveEnemy(StageInfo si, Vector3 activePos, Quaternion activeRot)
	{
		if (!dicEnemys.ContainsKey (si.ActiveModelIndex))
			return;

		List<EnemyBase> emList = dicEnemys [si.ActiveModelIndex.ToString()];
		if (emList == null && emList.Count <= 0)
			return;

		for (int i = 0; i < emList.Count; i++) 
		{
			if (emList [i].gameObject.activeSelf)
				continue;

			EnemyBase em = emList [i];

			GameObject enemyObj = em.gameObject;
			//em.SetEnemyInfo
			em.gameObject.transform.position = activePos;
			em.gameObject.transform.rotation = activeRot;
			em.gameObject.SetActive (true);
			em.SetPlaying ();
			em.SetEnemyMovePattern (si.MovePattern);
			break;
		}
	}

}
