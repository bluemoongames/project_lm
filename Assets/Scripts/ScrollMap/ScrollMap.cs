﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollMap : MonoBehaviour 
{

	const float DefaultScrollSpeed = 100.0f;

	bool bIsInit = false;
	public bool bIsFixedUpdate;

	public float ScrollSpeed;

	public Transform[] scrollObjs;
	bool bIsScrolling;
	float loopPosZ;

	void Awake()
	{
		Init ();
	}
		
	void Start () 
	{
		Init ();	
	}

	public void Init()
	{
		if (bIsInit)
			return;

		bIsScrolling = false;
		ScrollSpeed = DefaultScrollSpeed;
		SetScrollObjs ();
	}

	public void SetScrollObjs()
	{
		scrollObjs = new Transform[this.transform.childCount];
		for(int i = 0; i < this.transform.childCount; i++)
		{
			scrollObjs [i] = this.transform.GetChild (i).transform;
		}
		GameObject tempObj = scrollObjs [0].transform.FindChild ("Ground").gameObject;
		loopPosZ = tempObj.transform.localScale.z;

		bIsInit = true;
	}

	void Update () 
	{
		if (!bIsFixedUpdate)
			ScrollingMap (Time.deltaTime);
	}

	void FixedUpdate()
	{
		if (bIsFixedUpdate)
			ScrollingMap (Time.fixedDeltaTime);
	}

	public void ScrollingMap(float deltaTime)
	{
		if (!bIsScrolling)
			return;

		if (scrollObjs == null && scrollObjs.Length <= 0)
			return;

		for(int i = 0; i < scrollObjs.Length; i++)
		{
			scrollObjs [i].Translate (0, 0, deltaTime * ScrollSpeed * -1f);

			if(scrollObjs[i].transform.position.z <= -435)
			{
				scrollObjs [i].Translate (0, 0, loopPosZ * scrollObjs.Length);
			}
		}
	}

	public void ScrollingMap(bool b)
	{
		bIsScrolling = b;
	}
}
